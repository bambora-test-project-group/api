FROM openjdk:14-ea-slim-buster

RUN apt-get update && apt-get install -y wget
ADD ./build/libs/*.jar /app/

WORKDIR /app

CMD java -jar *.jar
