package com.horman.testproject.api

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/notify")
class CreditNotificationApi {
    companion object {
        private const val STATUS_OK = "OK"
    }

    @PostMapping("/credit")
    fun sendCredit(@RequestBody request: CreditRequest): CreditResponse {
        return CreditResponse(
            CreditResponseResult(
                request.params.signature, request.params.uuid, request.method,
                CreditResponseData(
                    STATUS_OK
                )
            ),
            request.version,
            request.id
        )
    }

    data class CreditResponse(
        val result: CreditResponseResult,
        val version: String,
        val id: Long
    )

    data class CreditResponseResult(
        val signature: String,
        val uuid: String,
        val method: String,
        val data: CreditResponseData
    )

    data class CreditResponseData(
        val status: String
    )

    data class CreditRequest(
        val method: String,
        val params: CreditRequestParams,
        val version: String,
        val id: Long
    )

    data class CreditRequestParams(
        val signature: String,
        val uuid: String,
        val data: CreditRequestData
    )

    data class CreditRequestData(
        val amount: String,
        val currency: String,
        @JsonProperty("messageid")
        val messageId: String,
        @JsonProperty("orderid")
        val orderId: String,
        @JsonProperty("enduserid")
        val endUserId: String,
        @JsonProperty("notificationid")
        val notificationId: String,
        val timestamp: String
    )
}
