package com.horman.testproject.api

import com.horman.testproject.service.TrustlyService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/trustly")
class TrustlyApi(private val trustlyService: TrustlyService) {

    @PostMapping("/deposit")
    suspend fun deposit(
        @RequestBody attributes: TrustlyService.DepositRequestDataAttributes,
        @RequestParam endUserId: String
    ): TrustlyService.DepositResultData {
        return trustlyService.deposit(attributes, endUserId)
    }
}
