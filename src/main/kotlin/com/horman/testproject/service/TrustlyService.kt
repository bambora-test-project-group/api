package com.horman.testproject.service

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.convertValue
import com.horman.testproject.exception.InvalidSignature
import com.horman.testproject.util.RandomIdGenerator
import com.horman.testproject.util.signer.RsaSigner
import com.horman.testproject.util.signer.Signer
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitExchange
import java.security.PrivateKey
import java.security.PublicKey
import java.util.*
import java.util.concurrent.atomic.AtomicLong


@Service
class TrustlyService(
    webClient: WebClient,
    objectMapper: ObjectMapper,
    @Qualifier("api_uri") private val apiUri: String,
    @Qualifier("trustly_prv_key") private val privateKey: PrivateKey,
    @Qualifier("notification_url") private val notificationUrl: String,
    @Qualifier("trustly_username") private val username: String,
    @Qualifier("trustly_password") private val password: String,
) {
    private val rpcConnector = RpcConnector(webClient, apiUri, objectMapper, privateKey)


    companion object {
        private const val JSON_RPC_VERSION = "1.1"
        private const val DEPOSIT_METHOD = "Deposit"
    }

    private fun createDepositRequestData(
        attributes: DepositRequestDataAttributes,
        endUserId: String
    ): DepositRequestData {
        val randomIdGenerator = RandomIdGenerator()
        val messageId = randomIdGenerator.generate()
        return DepositRequestData(username, password, notificationUrl, endUserId, messageId, attributes)
    }

    suspend fun deposit(
        attributes: DepositRequestDataAttributes,
        endUserId: String
    ): DepositResultData {
        val data = createDepositRequestData(attributes, endUserId)
        val resp = rpcConnector.callApi<DepositRequestData, DepositResult>(
            DEPOSIT_METHOD,
            data,
            object : TypeReference<RpcConnector.Response<DepositResult>>() {})
        if (resp.result != null) {
            return resp.result.data
        } else {
            return DepositResultData("0", "0")
        }

    }

    data class DepositRequestDataAttributes(
        @JsonProperty("Country")
        val country: String,
        @JsonProperty("Locale")
        val locale: String,
        @JsonProperty("Currency")
        val currency: String,
        @JsonProperty("IP")
        val ip: String,
        @JsonProperty("MobilePhone")
        val mobilePhone: String,
        @JsonProperty("Firstname")
        val firstName: String,
        @JsonProperty("Lastname")
        val lastName: String,
        @JsonProperty("Email")
        val email: String,
        @JsonProperty("NationalIdentificationNumber")
        val nationalIdentificationNumber: String,
        @JsonProperty("SuccessURL")
        val successUrl: String,
        @JsonProperty("FailURL")
        val failUrl: String,
    )

    data class DepositResultData(
        @JsonProperty("orderid")
        val orderId: String,
        val url: String
    )

    //region Private Models
    private data class DepositRequestData(
        @JsonProperty("Username")
        val username: String,
        @JsonProperty("Password")
        val password: String,
        @JsonProperty("NotificationURL")
        val notificationUrl: String,
        @JsonProperty("EndUserID")
        val endUserId: String,
        @JsonProperty("MessageID")
        val messageId: String,
        @JsonProperty("Attributes")
        val attributes: DepositRequestDataAttributes
    )

    private data class DepositResult(
        val signature: String,
        val uuid: String,
        val method: String,
        val data: DepositResultData
    )

    private class TrustlySignatureHandler(privateKey: PrivateKey, private val objectMapper: ObjectMapper) {
        private val signer: Signer =
            RsaSigner(privateKey, { Base64.getEncoder().encodeToString(this) }, { Base64.getDecoder().decode(this) })

        private fun serializeData(data: JsonNode): String {
            var str = ""
            val fieldNames = arrayListOf<String>()
            data.fields().forEach { field ->
                fieldNames.add(field.key)
            }
            fieldNames.sort()
            fieldNames.forEach { fieldName ->
                val objectNodeStr: String = if (data[fieldName] is ObjectNode) {
                    serializeData(data[fieldName])
                } else {
                    data[fieldName].textValue()
                }
                str = str + fieldName + objectNodeStr
            }
            return str
        }

        fun <T : Any> createSignature(method: String, uuid: String, data: T): String {
            val objectNode = objectMapper.convertValue<ObjectNode>(data)
            val plainText = method + uuid + serializeData(objectNode)
            val signature = signer.sign(plainText)
            if (signer.verify(plainText, signature)) {
                return signature
            } else {
                throw InvalidSignature()
            }

        }
    }

    private class RpcConnector(
        private val webClient: WebClient,
        private val apiUri: String,
        private val objectMapper: ObjectMapper,
        privateKey: PrivateKey
    ) {
        private val trustlySignatureHandler = TrustlySignatureHandler(privateKey, objectMapper)

        suspend fun <T : Any, L> callApi(
            method: String,
            data: T,
            typeReference: TypeReference<Response<L>>
        ): Response<L> {
            val uuid = UUID.randomUUID().toString()
            val signature = trustlySignatureHandler.createSignature(method, uuid, data)
            val params = Params(signature, uuid, data)
            val request = Request(method, params, JSON_RPC_VERSION)
            val respStr = webClient.post()
                .uri(apiUri)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .awaitExchange { resp ->
                    resp.awaitBody<String>()
                }
            return objectMapper.readValue(respStr, typeReference)
        }

        private data class Params<T>(
            @JsonProperty("Signature")
            val signature: String,
            @JsonProperty("UUID")
            val uuid: String,
            @JsonProperty("Data")
            val data: T
        )

        data class Request<T>(
            val method: String,
            val params: Params<T>,
            val version: String,
            val id: Long = this.i.incrementAndGet()
        ) {
            companion object {
                private val i = AtomicLong(0)
            }
        }

        data class Response<T>(val result: T?, val version: String, val id: Long)
    }
    //endregion
}
