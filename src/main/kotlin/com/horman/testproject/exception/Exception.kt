package com.horman.testproject.exception

open class BusinessException(
    open val code: String,
    open val description: String
) : Throwable("$code: $description", null, true, false)

class InvalidSignature : BusinessException("INVALID_SIGNATURE", "Request signature was not verified")
