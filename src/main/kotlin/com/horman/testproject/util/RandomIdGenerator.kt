package com.horman.testproject.util

import java.math.BigInteger
import java.security.SecureRandom

class RandomIdGenerator {
    fun generate(): String {
        val random = SecureRandom()
        return BigInteger(130, random).toString(32)
    }
}
