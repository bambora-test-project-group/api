package com.horman.testproject.util.signer

interface Signer {
    fun sign(msg: String): String
    fun verify(msg: String, signature: String): Boolean
}
