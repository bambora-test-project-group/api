package com.horman.testproject.util.signer

import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.Signature
import java.util.*
import java.security.spec.RSAPublicKeySpec

import java.security.interfaces.RSAPrivateCrtKey





class RsaSigner(
    private val privateKey: PrivateKey,
    private val convertToString: ByteArray.() -> String,
    private val convertToByteArray: String.() -> ByteArray
) :
    Signer {
    private val publicKey: PublicKey
    init {
        publicKey = getPublicKey()
    }
    override fun sign(msg: String): String {
        val signatureInstance = Signature.getInstance("SHA1withRSA")
        signatureInstance.initSign(privateKey)
        signatureInstance.update(msg.toByteArray())
        return signatureInstance.sign().convertToString()
    }

    override fun verify(msg: String, signature: String): Boolean {
        val signatureInstance = Signature.getInstance("SHA1withRSA")
        signatureInstance.initVerify(publicKey)
        signatureInstance.update(msg.toByteArray())
        val signatureBytes = signature.convertToByteArray()
        return signatureInstance.verify(signatureBytes)
    }

    private fun getPublicKey(): PublicKey {
        val rsaPrvKey = privateKey as RSAPrivateCrtKey
        val publicKeySpec = RSAPublicKeySpec(rsaPrvKey.modulus, rsaPrvKey.publicExponent)
        val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        return keyFactory.generatePublic(publicKeySpec)
    }
}
