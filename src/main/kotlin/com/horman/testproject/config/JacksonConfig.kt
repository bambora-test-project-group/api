package com.horman.testproject.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JacksonConfig {
    @Bean
    fun defaultObjectMapper(): ObjectMapper {
        return ObjectMapper()
            .registerModule(KotlinModule())
    }
}
