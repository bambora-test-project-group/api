package com.horman.testproject.config

import com.horman.testproject.util.Secrets
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.lang.RuntimeException
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*

@Configuration
class SecretsConfig {
    @Bean("api_uri")
    fun getTrustlyUri(): String {
        return Secrets.get("TRUSTLY_API_URI") ?: throw RuntimeException("Please, define variable of TRUSTLY_API_URI")
    }

    @Bean("notification_url")
    fun getNotificationUrl(): String {
        return Secrets.get("NOTIFICATION_URL") ?: throw RuntimeException("Please, define variable of NOTIFICATION_URL")
    }

    @Bean("trustly_username")
    fun getTrustlyUsername(): String {
        return Secrets.get("TRUSTLY_USERNAME") ?: throw RuntimeException("Please, define variable of TRUSTLY_USERNAME")
    }

    @Bean("trustly_password")
    fun getTrustlyPassword(): String {
        return Secrets.get("TRUSTLY_PASSWORD") ?: throw RuntimeException("Please, define variable of TRUSTLY_PASSWORD")
    }

    @Bean("trustly_prv_key")
    fun getPrivateKey(): PrivateKey {
        val privateKeyContent =
            Secrets.get("TRUSTLY_PRV_KEY") ?: throw RuntimeException("Please, define variable of TRUSTLY_PRV_KEY")
        val kf: KeyFactory = KeyFactory.getInstance("RSA")
        val keySpecPKCS8 = PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent))
        return kf.generatePrivate(keySpecPKCS8)
    }
}
