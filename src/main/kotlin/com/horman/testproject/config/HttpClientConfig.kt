package com.horman.testproject.config

import com.horman.testproject.util.springWebClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class HttpClientConfig {
    @Bean
    fun defaultWebClient(): WebClient {
        return springWebClient(
            connectTimeoutMs = 5000,
            readTimeoutMs = 5000,
            writeTimeoutMs = 5000,
            maxInMemorySize = 2 * 1024 * 1024,
        )
    }
}


